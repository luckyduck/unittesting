//
//  CalcTests.swift
//  UnitTesting
//
//  Created by Jan Brinkmann on 18.04.17.
//  Copyright © 2017 Jan Brinkmann. All rights reserved.
//

import XCTest
@testable import UnitTesting

class CalcTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testIstGerade() {
        let calc = Calc()
        let nummer = 8
        
        XCTAssertTrue(calc.istGerade(x: nummer))
    }
    
    func testIstUngerade() {
        let calc = Calc()
        let nummer = 9
        
        XCTAssertFalse(calc.istGerade(x: nummer))
    }
}
