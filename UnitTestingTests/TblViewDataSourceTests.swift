//
//  TblViewDataSourceTests.swift
//  UnitTesting
//
//  Created by Jan Brinkmann on 18.04.17.
//  Copyright © 2017 Jan Brinkmann. All rights reserved.
//

import XCTest
@testable import UnitTesting

class TblViewDataSourceTests: XCTestCase {
    
    var dataSource = TblViewDataSource()
    
    override func setUp() {
        super.setUp()
        
        dataSource = TblViewDataSource()
        dataSource.data = ["Test1", "Test2", "Test3"]
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testRowcount() {
        let expectedCount = 3
        
        XCTAssertEqual(dataSource.tableView(UITableView(), numberOfRowsInSection: 0), expectedCount)
    }
    
    func testFirstRow() {
        
        let indexPath = IndexPath(row: 0, section: 0)
        let cell = dataSource.tableView(UITableView(), cellForRowAt: indexPath)
        
        XCTAssertNotNil(cell.textLabel?.text)
        XCTAssertEqual(cell.textLabel?.text, dataSource.data[0])
    }
}
