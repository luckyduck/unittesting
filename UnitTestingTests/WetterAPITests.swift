//
//  WetterAPITests.swift
//  UnitTesting
//
//  Created by Jan Brinkmann on 18.04.17.
//  Copyright © 2017 Jan Brinkmann. All rights reserved.
//

import XCTest
@testable import UnitTesting

class WetterAPITests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testHeiss() {
        let expected = Wetter.heiss
        let adapter = MockAdapter(mockValue: 30)
        
        let api = WetterAPI(adapter: adapter)
        XCTAssertEqual(expected, api.checkWetter())
    }
 
    func testWarm() {
        let expected = Wetter.warm
        let adapter = MockAdapter(mockValue: 19)
        
        let api = WetterAPI(adapter: adapter)
        XCTAssertEqual(expected, api.checkWetter())
    }
    
    func testKalt() {
        let expected = Wetter.kalt
        let adapter = MockAdapter(mockValue: 15)
        
        let api = WetterAPI(adapter: adapter)
        XCTAssertEqual(expected, api.checkWetter())
    }
}
