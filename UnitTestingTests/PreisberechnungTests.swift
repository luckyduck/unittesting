//
//  PreisberechnungTests.swift
//  UnitTesting
//
//  Created by Jan Brinkmann on 18.04.17.
//  Copyright © 2017 Jan Brinkmann. All rights reserved.
//

import XCTest
@testable import UnitTesting

class PreisberechnungTests: XCTestCase {
    
    let ek = 10.0
    let rechner = Preisberechnung()
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testEndkundenpreis() {
        let expected = 17.85
        let calculated = rechner.getVK(ek: ek, klasse: .endkunde, steuer: .vollbesteuert)
        
        XCTAssertEqual(expected, calculated)
    }
    
    func testResellerMitMwSt() {
        let expected = 16.065
        let calculated = rechner.getVK(ek: ek, klasse: .reseller, steuer: .vollbesteuert)
        
        XCTAssertEqual(expected, calculated)
    }
    
    func testResellerOhneMwSt() {
        let expected = 13.5
        let calculated = rechner.getVK(ek: ek, klasse: .reseller, steuer: .steuerbefreit)
        
        XCTAssertEqual(expected, calculated)
    }
}
