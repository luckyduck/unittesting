//
//  Calc.swift
//  UnitTesting
//
//  Created by Jan Brinkmann on 18.04.17.
//  Copyright © 2017 Jan Brinkmann. All rights reserved.
//

import Foundation

class Calc {
    func istGerade(x: Int) -> Bool {
        if x % 2 == 1 {
            return false
        }
        
        return true
    }
}
