//
//  WetterAPI.swift
//  UnitTesting
//
//  Created by Jan Brinkmann on 18.04.17.
//  Copyright © 2017 Jan Brinkmann. All rights reserved.
//

import Foundation

class WetterAPI {
    var adapter: BaseAPI
    
    init(adapter: BaseAPI) {
        self.adapter = adapter
    }
    
    func checkWetter() -> Wetter {
        let temp = adapter.getTemperatur()
        
        if temp > 28 {
            return .heiss
        } else if temp > 18 {
            return .warm
        }
        
        return .kalt
    }
}
