//
//  JSONAdapter.swift
//  UnitTesting
//
//  Created by Jan Brinkmann on 18.04.17.
//  Copyright © 2017 Jan Brinkmann. All rights reserved.
//

import Foundation

class JSONAdapter: BaseAPI {
    
    func getTemperatur() -> Double {
        // json download von domain.xyz
        print("JSON: heruntergeladen")
        print("JSON: Werte auslesen")
        let temp = 18.1
        
        return temp
    }
}
