//
//  Preisberechnung.swift
//  UnitTesting
//
//  Created by Jan Brinkmann on 18.04.17.
//  Copyright © 2017 Jan Brinkmann. All rights reserved.
//

import Foundation

enum Preisklasse: Int {
    case endkunde = 0
    case reseller = 10
}

enum Umsatzsteuer: Int {
    case steuerbefreit = 0
    case vollbesteuert = 19
}

class Preisberechnung {
    
    func getVK(ek: Double, klasse: Preisklasse, steuer: Umsatzsteuer) -> Double {
        // 50%: 5€ Einkauf => 10€ Verkauf
        var vk = ek * 1.5
        
        // Rabatt auf VK
        let rabattProzent = Double(klasse.rawValue)
        let rabatt = vk / 100 * rabattProzent
        vk -= rabatt
        
        // Steuer
        let steuersatz = Double(steuer.rawValue)
        let steuerbetrag = vk / 100 * steuersatz
        vk += steuerbetrag
        
        return vk
    }
}
