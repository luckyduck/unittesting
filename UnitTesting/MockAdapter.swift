//
//  MockAdapter.swift
//  UnitTesting
//
//  Created by Jan Brinkmann on 18.04.17.
//  Copyright © 2017 Jan Brinkmann. All rights reserved.
//

import Foundation

class MockAdapter: BaseAPI {
    var mockValue = 20.0
    
    init(mockValue: Double) {
        self.mockValue = mockValue
    }
    
    func getTemperatur() -> Double {
        return mockValue
    }
}
