//
//  BaseAPI.swift
//  UnitTesting
//
//  Created by Jan Brinkmann on 18.04.17.
//  Copyright © 2017 Jan Brinkmann. All rights reserved.
//

import Foundation

enum Wetter {
    case kalt
    case warm
    case heiss
}

protocol BaseAPI {
    func getTemperatur() -> Double
}
